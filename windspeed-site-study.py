''' Example Site Study 
    ================== 
    Look at ways to unpack data from a specific site and apply
    simulated change estimates.
    * Use monthly mean wind speed, both because that was the original
      client request and also because it's easier/faster to work with. 
      Will need to expand this capacity.
    * Worth noting that all of this is potentially of suspect value
      compared to more conceptual synoptic analysis.
                                                                             '''
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import xarray as xr
import glob as grod
import cftime

from pandas.plotting import register_matplotlib_converters
from sklearn.preprocessing import QuantileTransformer
from sklearn.neighbors import KernelDensity
from scipy.interpolate import CubicSpline

register_matplotlib_converters()
cmap = sns.color_palette('Set2')
sns.set_style('darkgrid')
sns.set_palette('Set2')

month_labs=[
    'January','February', 'March','April',
    'May','June','July','August',
    'September','October','November','December']

''' Site series
    -----------
    Identify the document and table that will consider in this example.
                                                                             '''
print('_________________')
print('Reading site data')

site_loc = (0.08,53.48)             # tuple: (lon,lat) 
path_site = '/home/ubuntu/UKsite'
           #'../data/UKsite' 
fname_site = path_site+'/Donna_nook_and_other.xlsx'
lab_windspeed = "Wind Speed scaled Donna Nook [m/s]"

D = pd.read_excel(fname_site,sheet_name='Donna Nook')
D.index = pd.to_datetime(D['Date/Time'].values)

''' Sim series
    ----------
    1) Identify simulation files to use in the example, 
    2) Extract series from specific site
    3) Estimate wind speed
    4) Calculate monthly means
                                                                             '''
print('__________________')
print('Reading sim values')

path_sims = '/home/ubuntu/eucordex/NorthSea'
           #'../data' 

file_list = grod.glob(path_sims+'/NorthSea_*uas*historical*.nc')
sim_labs = []
for f in file_list :
    x = f.split('_')
    x[1] = '%s'
    x[4] = '%s'
    sim_labs += [('_').join(x)]

sim_labs_0 = [
    (path_sims+'/NorthSea_%s_EUR-11_NCC-NorESM1-M'
     + '_%s_r1i1p1_DMI-HIRHAM5_v2_day.nc'),
    (path_sims+'/NorthSea_%s_EUR-11_MPI-M-MPI-ESM-LR'
      +'_%s_r1i1p1_CLMcom-CCLM4-8-17_v1_day.nc')]

# ____________________________________________________________________________ #
def extract_ws_series(site_loc,sim_lab,scenario='historical') :
    ''' 
    Pull time series of wind speed from forecasts. 
  
    Variables
    ---------
    site_loc : lon,lat coords of site to extract (tuple)
    sim_lab  : string base of file name (str) 
    scenario : historical or rcp85 (str)
    '''
    
    X = xr.open_dataset(
        (sim_labs[ix_sim] % ('uas',scenario)))
    dist_matr = np.sqrt(
        ((X.lat.values-site_loc[1])**2)
        + ((X.lon.values-site_loc[0])**2))
    u = pd.DataFrame(
        X.uas.values[:,(dist_matr == dist_matr.min())],
        index=X.time.values, 
        columns=['uas'])
    if ((type(u.index[0]) == cftime._cftime.DatetimeNoLeap)
        | (type(u.index[0]) == cftime._cftime.Datetime360Day) ) :
        tlab = [str(x.year).zfill(4)
                + '-'
                + str(x.month).zfill(2)
                + '-'+str(x.day).zfill(2)
                for x in u.index]
        u.index = pd.to_datetime(tlab,errors='coerce')
    else :
        u.index = pd.to_datetime(u.index)
    
    X = xr.open_dataset(
        (sim_labs[ix_sim] % ('vas',scenario)))
    dist_matr = np.sqrt(
        ((X.lat.values-site_loc[1])**2)
        + ((X.lon.values-site_loc[0])**2))
    v = pd.DataFrame(
        X.vas.values[:,(dist_matr == dist_matr.min())],
        index=X.time.values, 
        columns=['vas'])
    if ((type(v.index[0]) == cftime._cftime.DatetimeNoLeap)
        | (type(v.index[0]) == cftime._cftime.Datetime360Day) ) :
        tlab = [str(x.year).zfill(4)
                + '-'
                + str(x.month).zfill(2)
                + '-'+str(x.day).zfill(2)
                for x in v.index]
        v.index = pd.to_datetime(tlab,errors='coerce')

    else :
        v.index = pd.to_datetime(v.index)
    
    x = pd.merge(
        left=u,
        right=v,
        how='outer',
        left_index=True,
        right_index=True)
    ws = pd.DataFrame(
        np.sqrt((x.uas.values**2)
                +(x.vas.values**2)),
        index=x.index,
        columns=['windspeed'])

    return(ws)
# ____________________________________________________________________________ #

for ix_sim in range(len(sim_labs)) :
    print('* Reading values for '+sim_labs[ix_sim])
    lab = 'Sim'+str(ix_sim).zfill(2)
    ws = pd.concat([
        extract_ws_series(
            site_loc=site_loc,
            sim_lab=sim_labs[ix_sim],
            scenario='historical'),
        extract_ws_series(
            site_loc=site_loc,
            sim_lab=sim_labs[ix_sim],
            scenario='rcp85')])
    ws.columns = [lab]
    ws = ws.resample('M').mean().dropna()
    if ix_sim == 0 : 
        S_ws = ws
    else : 
        S_ws = pd.merge(
            left=S_ws,right=ws,
            how='outer',
            left_index=True,
            right_index=True)
 
## Montly mean wind speed 
Xs = S_ws.resample('M').mean().dropna()

''' Bias Corrections
    ----------------
    Can nudge the two series close, but still don't get the best match
    for range, or shape of the distribution.So instead will use
    quantile mapping by setting an the inverse transform to convert
    any value within the range of the original "B" data set to it's
    "A" equivilant, and then fitting a cubic spline interpolation to
    define the resulting curve over the full potential value range.
    * Should note, quantile mapping can be a very very bad idea in
      many cases. There are many issues, but most notable is that it
      caps the possible values within the range of the observed
      values. For a situation where we expect a shift in values to one
      end or the other of the current observed range (e.g., strong
      winds happening more often) then this is a functional bias
      correction. If we suspect the potential for changes that will
      change the range of possible values (e.g., tropical level
      rainfalls in regions that previously had different precipitation
      generating mechanisms) then this approach can be periously
      missleading.
                                                                             '''
print('_______________________')
print('Running bias correction')

## Percentiles
X = D[lab_windspeed].resample('M').mean().dropna().values
_ = plt.plot(
    np.sort(X),
    (np.sort(X)/np.sum(X)).cumsum(),
    linewidth=4,
    label='Obs')

nq = 100
ix_hist = (Xs.index.year < 2010)
Xr = Xs.copy(deep=True)

for sim_lab in S_ws.columns : 
    
    ## Create distinct transform-objects for each data set
    Xd = D[lab_windspeed].resample('M').mean().dropna()
    qt0 = QuantileTransformer(n_quantiles=nq, random_state=0)
    _ = qt0.fit(Xd.values.reshape(-1, 1))
    qt1 = QuantileTransformer(n_quantiles=nq, random_state=0)
    _ = qt1.fit(Xs.loc[ix_hist,sim_lab].values.reshape(-1,1))

    ## Evaluate quantile map function
    z = np.linspace(Xs.min().min()-5,Xs.max().max()+5,1000)
    z1 = qt1.transform(z.reshape(-1,1))
    zt = qt0.inverse_transform(z1)
    cs = CubicSpline(
        z,zt[:,0],
        extrapolate=False,
        bc_type='natural'
    )
    
    Xr.loc[:,sim_lab] = cs(Xs.loc[:,sim_lab])
    

# =:<>:==:<>:==:<>:==:<>:==:<>:==:<>:==:<>:==:<>:==:<>:==:<>:==:<>:==:<>:==:<>:=

''' Distribution change plots
    -------------------------

    The top plane in the following plots is the difference between the
    KDE estimate for the projection and the historical period, and
    then the bottom plane contains box plots showing the range of mean
    values for the period from across the ensemble
                                                                             '''
print('_____________________________________')
print('Creating climatology difference plots')
image_dir = '../images'
x = np.arange(0,10,0.01)
for m in range(1,13) :
    print('* processing values for '+month_labs[m-1])
    fig,chart = plt.subplots(
        nrows=2,
        gridspec_kw={
            'height_ratios': [2,1]},
        sharex=True)
    Kd = pd.DataFrame(
        index=x,
        columns=Xr.columns)
    Mu = pd.DataFrame(
        index=Xr.columns,
        columns=['hist','proj'])
    ## Interannual variation and mean for each simulation
    for sim_lab in Xr.columns :
        ## Estimate values
        ix = [((x.year in list(range(1976,2006))) 
               & (x.month==m)) 
              for x in Xs.index]
        Xh = Xr.loc[ix,sim_lab].values
        estm = KernelDensity(
            kernel='gaussian',bandwidth=0.5
        ).fit(Xh.reshape(-1, 1))
        Kh = np.exp(estm.score_samples(x.reshape(-1,1)))
        Mu.loc[sim_lab,'hist'] = Xh.mean()
        ix = [((x.year in list(range(2021,2051))) 
               & (x.month==m)) 
              for x in Xs.index]
        Xf = Xr.loc[ix,sim_lab].values
        estm = KernelDensity(
            kernel='gaussian',bandwidth=0.5
        ).fit(Xf.reshape(-1, 1))
        Kf = np.exp(estm.score_samples(x.reshape(-1,1)))
        Mu.loc[sim_lab,'proj'] = Xf.mean()
        Kd.loc[:,sim_lab] = Kf-Kh
    ## Draw values
    _ = chart[0].set_title('')
    _ = chart[0].plot(
        Kd,
        color=cmap[2])
    _ = chart[0].set_xlim(0,10)
    _ = chart[0].set_ylabel('Density Difference')
    bplot = chart[1].boxplot(
        Mu.T,
        vert=False,
        widths=0.5,
        whis=200,
        patch_artist=True)
    _ = bplot['boxes'][0].set(facecolor=cmap[0],alpha=0.8)
    _ = bplot['medians'][0].set(color='black')
    _ = bplot['boxes'][0].set(facecolor=cmap[1],alpha=0.8)
    _ = bplot['medians'][1].set(color='black')
    _ = chart[1].set_xlim(0,10)
    _ = chart[1].set_ylim(0.5,2.5)
    _ = chart[1].set_yticks([1,2])
    _ = chart[1].set_yticklabels(['Hist','Proj'])
    _ = chart[1].set_ylabel('Mean values')
    _ = chart[1].set_xlabel('Wind Speed (m/s)')
    _ = plt.suptitle(month_labs[m-1],fontsize=16)
    _ = plt.savefig(
        image_dir
        + '/clim_monthlymean_windspeed_'
        + month_labs[m-1]
        + '.png')
    _ = plt.close()
    

''' Relative change plots
    ---------------------
    The above plots express the variation, but might be a bit too busy
    to pull values out of, so also do a set of box plots showing the
    intra ensemble variability and change relative to the historical
    climatology mean.
                                                                             '''
print('_____________________________________')
print('Creating relative difference plots')
image_dir = '../images'
for m in range(1,13) :
    print('* processing values for '+month_labs[m-1])
    ## Set Storage
    fig,chart = plt.subplots(nrows=1,figsize=(9,3))
    Mu = pd.DataFrame(
        index=Xr.columns,
        columns=['hist','proj'])
    ## Interannual variation and mean for each simulation
    for sim_lab in Xr.columns :
        ## Estimate values
        ix = [((x.year in list(range(1976,2006))) 
               & (x.month==m)) 
              for x in Xs.index]
        Xh = Xr.loc[ix,sim_lab].values
        Mu.loc[sim_lab,'hist'] = Xh.mean()
        ix = [((x.year in list(range(2021,2051))) 
               & (x.month==m)) 
              for x in Xs.index]
        Xf = Xr.loc[ix,sim_lab].values
        Mu.loc[sim_lab,'proj'] = Xf.mean()
    Mu = ((Mu-Mu.loc[:,'hist'].mean())/Mu.loc[:,'hist'].mean())*100
    ## Draw values
    bplot = chart.boxplot(
        Mu.T,
        vert=False,
        widths=0.5,
        whis=200,
        patch_artist=True)
    _ = bplot['boxes'][0].set(facecolor=cmap[0],alpha=0.8)
    _ = bplot['medians'][0].set(color='black')
    _ = bplot['boxes'][0].set(facecolor=cmap[1],alpha=0.8)
    _ = bplot['medians'][1].set(color='black')
    _ = chart.set_xlim(-50,50)
    _ = chart.set_ylim(0.5,2.5)
    _ = chart.set_yticks([1,2])
    _ = chart.set_yticklabels(['Hist','Proj'])
    _ = chart.set_ylabel('Clim Mean Values')
    _ = chart.set_xlabel('Relative Change (%)')
    _ = plt.title(month_labs[m-1],fontsize=16)
    _ = plt.tight_layout()
    _ = plt.savefig(
        image_dir
        + '/relative_change_monthlymean_windspeed_'
        + month_labs[m-1]
        + '.png')
    _ = plt.close()
    
